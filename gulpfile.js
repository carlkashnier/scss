require('es6-promise').polyfill();
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    watch = require('gulp-watch'),
    rename = require('gulp-rename');

gulp.task('connect', function () {
    connect.server();
});


gulp.task('sass', function () {
    gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 1%'],
            cascade: false
        }))
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./'))
        .pipe(livereload());
});

gulp.task('sass:watch', function () {
    livereload.listen();
    gulp.watch('./SCSS/**/*.scss', ['sass']);
});
gulp.task('php:watch', function () {
    livereload.listen();
    gulp.watch('./**/*.php', livereload.reload)
});
gulp.task('html:watch', function () {
    livereload.listen();
    gulp.watch('./*.html', livereload.reload)
});

gulp.task('libs', function () {
    return gulp.src([
        // file paths go here
    ])
        .pipe(concat('lib.js'))
        .pipe(gulp.dest('js/dist/'));
});

gulp.task('app', function () {
    return gulp.src([
        'js/src/*.js'
    ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('js/dist/'))
});

gulp.task('app:watch', function () {
    return gulp.watch([
        'JS/*.js',
    ], ['app']);
});
gulp.task('dev', ['sass', 'sass:watch', 'libs', 'app', 'app:watch', 'connect', 'php:watch']);
gulp.task('default', ['sass', 'libs', 'app']);

